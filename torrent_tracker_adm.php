<?php

class TTSettingsPage {

    private $options;

    public function __construct() {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    public function add_plugin_page() {
        add_options_page(
                'Settings Admin', 'Torrent Tracker', 'manage_options', 'tt-setting-admin', array($this, 'create_admin_page')
        );
    }

    public function create_admin_page() {
        $this->options = get_option('21h_torrent_tracker');
        ?>
        <div class="wrap">
            <h2>Torrent tracker</h2>           
            <form method="post" action="options.php">
        <?php
        settings_fields('option_group1');
        do_settings_sections('tt-setting-admin');
        submit_button();
        ?>
            </form>

        </div>
                <?php
            }

            public function page_init() {
                register_setting(
                        'option_group1', '21h_torrent_tracker', array($this, 'sanitize')
                );
                add_settings_section(
                        'setting_section_id', 'View settings', array($this, 'print_section_info1'), 'tt-setting-admin'
                );

                add_settings_field(
                        'viewas', 'View list as table', array($this, 'viewas_callback'), 'tt-setting-admin', 'setting_section_id'
                );

                add_settings_field(
                        'showseed', 'Show seeders', array($this, 'showseed_callback'), 'tt-setting-admin', 'setting_section_id'
                );

                add_settings_field(
                        'showleech', 'Show leechers', array($this, 'showleech_callback'), 'tt-setting-admin', 'setting_section_id'
                );

                add_settings_field(
                        'torrents_per_page', 'Torrents per page (0 for all in one page)', array($this, 'torrents_per_page_callback'), 'tt-setting-admin', 'setting_section_id'
                );
                add_settings_field(
                        'cachetime', 'Caching seeders and leechers (sec)', array($this, 'cachetime_callback'), 'tt-setting-admin', 'setting_section_id'
                );

                add_settings_field(
                        'html_before', 'HTML before', array($this, 'html_before_callback'), 'tt-setting-admin', 'setting_section_id'
                );
                add_settings_field(
                        'html_after', 'HTML after', array($this, 'html_after_callback'), 'tt-setting-admin', 'setting_section_id'
                );
                add_settings_field(
                        'html_tracker_title', 'Tracker title', array($this, 'html_tracker_title_callback'), 'tt-setting-admin', 'setting_section_id'
                );
                add_settings_field(
                        'html_in_post_tracker_title', 'In-post tracker title', array($this, 'html_in_post_tracker_title_callback'), 'tt-setting-admin', 'setting_section_id'
                );
            }

            public function sanitize($input) {
                $new_input = array();
                if (isset($input['torrents_per_page']))
                    $new_input['torrents_per_page'] = absint($input['torrents_per_page']);

                if (isset($input['viewas']))
                    $new_input['viewas'] = 'checked';

                if (isset($input['showseed']))
                    $new_input['showseed'] = 'checked';

                if (isset($input['showleech']))
                    $new_input['showleech'] = 'checked';

                if (isset($input['cachetime']))
                    $new_input['cachetime'] = absint($input['cachetime']);

                if (isset($input['html_before']))
                    $new_input['html_before'] = $input['html_before'];

                if (isset($input['html_after']))
                    $new_input['html_after'] = $input['html_after'];

                if (isset($input['html_tracker_title']))
                    $new_input['html_tracker_title'] = $input['html_tracker_title'];

                if (isset($input['html_in_post_tracker_title']))
                    $new_input['html_in_post_tracker_title'] = $input['html_in_post_tracker_title'];

                return $new_input;
            }

            public function print_section_info1() {
                print 'Check write permissions to plugin directory if something goes wrong. Updates and support only <a href="http://blindage.org/?p=7686">here</a>.';//.esc_attr(print_r($this->options,true));
            }

            public function torrents_per_page_callback() {
                printf(
                        '<input type="text" id="torrents_per_page" name="21h_torrent_tracker[torrents_per_page]" value="%s" style="width:50px" />', isset($this->options['torrents_per_page']) ? esc_attr($this->options['torrents_per_page']) : ''
                );
            }

            public function viewas_callback() {
                printf(
                        '<input type="checkbox" id="viewas" name="21h_torrent_tracker[viewas]" %s />', isset($this->options['viewas']) ? esc_attr($this->options['viewas']) : ''
                );
            }

            public function showseed_callback() {
                printf(
                        '<input type="checkbox" id="showseed" name="21h_torrent_tracker[showseed]" %s />', isset($this->options['showseed']) ? esc_attr($this->options['showseed']) : ''
                );
            }

            public function showleech_callback() {
                printf(
                        '<input type="checkbox" id="showleech" name="21h_torrent_tracker[showleech]" %s />', isset($this->options['showleech']) ? esc_attr($this->options['showleech']) : ''
                );
            }

            public function cachetime_callback() {
                printf(
                        '<input type="text" id="cachetime" name="21h_torrent_tracker[cachetime]" value="%s" style="width:50px" />', isset($this->options['cachetime']) ? esc_attr($this->options['cachetime']) : ''
                );
            }

            public function html_before_callback() {
                printf(
                        '<input type="text" id="html_before" name="21h_torrent_tracker[html_before]" value="%s" style="width:350px" />', isset($this->options['html_before']) ? esc_attr($this->options['html_before']) : ''
                );
            }

            public function html_after_callback() {
                printf(
                        '<input type="text" id="html_after" name="21h_torrent_tracker[html_after]" value="%s" style="width:350px" />', isset($this->options['html_after']) ? esc_attr($this->options['html_after']) : ''
                );
            }
            
            public function html_tracker_title_callback() {
                printf(
                        '<input type="text" id="html_tracker_title" name="21h_torrent_tracker[html_tracker_title]" value="%s" style="width:350px" />', isset($this->options['html_tracker_title']) ? esc_attr($this->options['html_tracker_title']) : ''
                );
            }

            public function html_in_post_tracker_title_callback() {
                printf(
                        '<input type="text" id="html_in_post_tracker_title" name="21h_torrent_tracker[html_in_post_tracker_title]" value="%s" style="width:350px" />', isset($this->options['html_in_post_tracker_title']) ? esc_attr($this->options['html_in_post_tracker_title']) : ''
                );
            }            

        }

        if (is_admin())
            $my_settings_page = new TTSettingsPage();
