<?php

/*
  Plugin Name: 21h Torrent Tracker
  Text Domain: blindage.org
  Domain Path: /
  Plugin URI: http://blindage.org/?p=7686
  Description: Torrent tracker
  Author: Vladimir Smagin
  Author URI: http://blindage.org/
  Version: 1.1
  License: GPL
  Donate URI: http://blindage.org
 */

/**
  License:
  ==============================================================================
  Copyright 2015 Vladimir Smaign  (email : 21h@blindage.org)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 */
function ttdb_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . "torrent_tracker";

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  file_id mediumint(9) NOT NULL,
  title varchar(255) DEFAULT '' NOT NULL,
  url varchar(255) DEFAULT '' NOT NULL,
  cached_path varchar(255) DEFAULT '' NOT NULL,
  size_mb mediumint(9) NOT NULL,
  sha1 varchar(255) NOT NULL,
  announce varchar(255),
  scrape varchar(255),
  cached datetime DEFAULT '00-00-0000 00:00:00' NOT NULL,
  seeders mediumint(9) NOT NULL,
  leechers mediumint(9) NOT NULL,
  UNIQUE KEY id (id)
) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
    add_option("21h_torrent_tracker_db_version", "1.0");
    add_option('21h_torrent_tracker', array(
        "torrents_per_page" => 0,
        "viewas" => 'checked',
        "showseed" => 'checked',
        "showleech" => 'checked',
        "cachetime" => 600,
        "html_before" => "<div id='torrent_list'>",
        "html_after" => "</div>",
        "html_tracker_title" => "<h3>All torrent files</h3>",
        "html_in_post_tracker_title" => "<h3>All torrent files in this post</h3>"
    ));
}

function ttdb_select_all() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'torrent_tracker';
    return $wpdb->get_results("SELECT * FROM $table_name order by title asc", ARRAY_A);
}

function ttdb_select_id($id) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'torrent_tracker';
    return $wpdb->get_results("SELECT * FROM $table_name where file_id=$id order by title asc", ARRAY_A);
}

function ttdb_insert($data) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'torrent_tracker';
    $wpdb->insert($table_name, $data);
}

function ttdb_update($id, $data) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'torrent_tracker';
    $wpdb->update($table_name, $data, array('file_id' => $id));
}

function ttdb_delete($id) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'torrent_tracker';
    $wpdb->delete($table_name, array('file_id' => $id));
}

function search_for_torrents($post_id) {
    $attachs = get_attached_media('application/torrent', $post_id);
    $return = array();
    foreach ($attachs as $attach) {
        $title = $attach->post_title;
        $url = $attach->guid;
        $file_id = $attach->ID;
        array_push($return, array('title' => $title, 'url' => $url, 'file_id' => $file_id));
    }
    return $return;
}

function db_torrent_exists($db, $file_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'torrent_tracker';
    $count = $wpdb->get_var("SELECT COUNT(id) FROM $table_name where file_id=" . $file_id);
    return $count;
}

function torrent_add_attachment($post_ID) {
    $post = get_post($post_ID);
    $upload_dir = wp_upload_dir()['path'];

    if (strpos($post->post_mime_type, 'torrent') and db_torrent_exists($db, $post_ID) == 0) {
        $torrent_file_contents = file_get_contents($upload_dir . '/' . basename($post->guid));
        $cached_path = 'cache/' . sha1($torrent_file_contents);
        $info = bdecode($torrent_file_contents);
        $sha1 = sha1(bencode($info['info']), true);
        $announce = $info['announce'];
        $scrape = str_replace("announce ", "scrape", $announce);
        $cached = date("Y-m-d H:i:s");
        $size_mb = 0;
        if (isset($info['info']['files'])) {
            foreach ($info['info']['files'] as $file) {
                $size_mb = $size_mb + round($file['length'] / 1024 / 1024);
            }
        } else {
            $size_mb = $size_mb + round($info['info']['length'] / 1024 / 1024);
        }
        file_put_contents(plugin_dir_path(__FILE__) . $cached_path, $torrent_file_contents);

        $data = array(
            'file_id' => $post_ID,
            'title' => $post->post_title,
            'url' => $post->guid,
            'cached_path' => $cached_path,
            'sha1' => base64_encode($sha1),
            'size_mb' => $size_mb,
            'announce' => $announce,
            'scrape' => $scrape,
            'cached' => $cached
        );

        ttdb_insert($data);
    }
}

function torrent_delete_attachment($post_ID) {
    ttdb_delete($post_ID);
}

function torrent_edit_attachment($post_ID) {
    $post = get_post($post_ID);
    $data = array(
        'title' => $post->post_title
    );
    ttdb_update($post_ID, $data);
}

function selectFormOfView() {
	$options = get_option('21h_torrent_tracker');
	if (isset($options['viewas'])) { return generateTrackerHTMLasTable(); } else { return generateTrackerHTMLasList(); }
}

function generateTrackerHTMLasList() {
    $options = get_option('21h_torrent_tracker');
    $torrents = ttdb_select_all();
    $htmlcode = $options['html_before'];
    $htmlcode.= $options['html_tracker_title'];
    $htmlcode.="<ol>";
    foreach ($torrents as $torrent) {
        if (time() - strtotime($torrent['cached']) > $options['cachetime']) {
            $scrape_url = $torrent['scrape'] . '?info_hash=' . urlencode(base64_decode($torrent['sha1']));

            if (($torrent_file_contents = @file_get_contents($scrape_url)) === FALSE) {
                //tracker not available
            } else {
                $sha1 = base64_decode($torrent['sha1']);
                $scrape_data = bdecode($torrent_file_contents);

                if (isset($scrape_data['files'][$sha1]['complete'])) {
                    $seeders = $scrape_data['files'][$sha1]['complete'];
                    $leechers = $scrape_data['files'][$sha1]['incomplete'];
                } else {
                    $seeders = $scrape_data['complete'];
                    $leechers = $scrape_data['incomplete'];
                }
                $data = array(
                    'seeders' => $seeders,
                    'leechers' => $leechers,
                    'cached' => date('Y-m-d H:i:s')
                );
                ttdb_update($torrent['file_id'], $data);
                $htmlcode.="<li>";
                $htmlcode.="<a href = '" . $torrent['url'] . "' > " . $torrent['title'] . "</a> ";
                $htmlcode.=" (" . $torrent['size_mb'] . " Mb) ";
                if (isset($options['showseed'])) $htmlcode.="<span class='tt_seeds'>Seeders: $seeders</span> ";
				if (isset($options['showleech'])) $htmlcode.="<span class='tt_leech'>Leechers: $leechers</span> ";
                $htmlcode.="</li>";
            }
        } else {
            $sha1 = base64_decode($torrent['sha1']);
            $scrape_data = bdecode($torrent_file_contents);
            $seeders = $torrent['seeders'];
            $leechers = $torrent['leechers'];
                $htmlcode.="<li>";
                $htmlcode.="<a href = '" . $torrent['url'] . "' > " . $torrent['title'] . "</a> ";
                $htmlcode.=" (" . $torrent['size_mb'] . " Mb) ";
                if (isset($options['showseed'])) $htmlcode.="<span class='tt_seeds'>Seeders: $seeders</span> ";
				if (isset($options['showleech'])) $htmlcode.="<span class='tt_leech'>Leechers: $leechers</span> ";
                $htmlcode.="</li>";
        }
    }
    $htmlcode.="</ol>";
    $htmlcode.=$options['html_after'];
    return $htmlcode;
}

function generateTrackerHTMLasTable() {
    $options = get_option('21h_torrent_tracker');
    $torrents = ttdb_select_all();
    $htmlcode = $options['html_before'];
    $htmlcode.= $options['html_tracker_title'];
    $htmlcode.="<table border='1' cellspacing='1'>";
    $htmlcode.="<tr><th>Title</th><th>Size (Mb)</th>";
    if (isset($options['showseed'])) $htmlcode.="<th>Seeders</th>";
    if (isset($options['showleech'])) $htmlcode.="<th>Leechers</th>";
    $htmlcode.="</tr>";
    foreach ($torrents as $torrent) {
        if (time() - strtotime($torrent['cached']) > $options['cachetime']) {
            $scrape_url = $torrent['scrape'] . '?info_hash=' . urlencode(base64_decode($torrent['sha1']));

            if (($torrent_file_contents = @file_get_contents($scrape_url)) === FALSE) {
                //tracker not available
            } else {
                $sha1 = base64_decode($torrent['sha1']);
                $scrape_data = bdecode($torrent_file_contents);

                if (isset($scrape_data['files'][$sha1]['complete'])) {
                    $seeders = $scrape_data['files'][$sha1]['complete'];
                    $leechers = $scrape_data['files'][$sha1]['incomplete'];
                } else {
                    $seeders = $scrape_data['complete'];
                    $leechers = $scrape_data['incomplete'];
                }
                $data = array(
                    'seeders' => $seeders,
                    'leechers' => $leechers,
                    'cached' => date('Y-m-d H:i:s')
                );
                ttdb_update($torrent['file_id'], $data);
                $htmlcode.="<tr>";
                $htmlcode.="<td><a href = '" . $torrent['url'] . "' > " . $torrent['title'] . "</a></td>";
                $htmlcode.="<td>" . $torrent['size_mb'] . "</td>";
                if (isset($options['showseed'])) $htmlcode.="<td>$seeders</td>";
                if (isset($options['showleech'])) $htmlcode.="<td>$leechers</td>";
                $htmlcode.="</tr>";
            }
        } else {
            $sha1 = base64_decode($torrent['sha1']);
            $scrape_data = bdecode($torrent_file_contents);
            $seeders = $torrent['seeders'];
            $leechers = $torrent['leechers'];
                $htmlcode.="<tr>";
                $htmlcode.="<td><a href = '" . $torrent['url'] . "' > " . $torrent['title'] . "</a></td>";
                $htmlcode.="<td>" . $torrent['size_mb'] . "</td>";
                if (isset($options['showseed'])) $htmlcode.="<td>$seeders</td>";
                if (isset($options['showleech'])) $htmlcode.="<td>$leechers</td>";
                $htmlcode.="</tr>";
        }
    }
    $htmlcode.="</table>";
    $htmlcode.=$options['html_after'];
    return $htmlcode;
}

function generateTrackerPostHTML() {
    $post_ID = $GLOBALS['post']->ID;
    $options = get_option('21h_torrent_tracker');

    $htmlcode = $options['html_before'];
    $htmlcode.= $options['html_in_post_tracker_title'];
    $htmlcode.="<ol>";

    $torrents_in_post = search_for_torrents($post_ID);

    foreach ($torrents_in_post as $torrent_in_post) {
        if (db_torrent_exists($db, $torrent_in_post['file_id']) > 0) {
            //get only registered in DB torrent files
            $torrent = ttdb_select_id($torrent_in_post['file_id']);

            if (time() - strtotime($torrent['cached']) > $options['cachetime']) {
                $scrape_url = $torrent['scrape'] . '?info_hash=' . urlencode(base64_decode($torrent['sha1']));

                if (($torrent_file_contents = @file_get_contents($scrape_url)) === FALSE) {
                    //tracker not available
                } else {
                    $sha1 = base64_decode($torrent['sha1']);
                    $scrape_data = bdecode($torrent_file_contents);
                    if (isset($scrape_data['files'][$sha1]['complete'])) {
                        $seeders = $scrape_data['files'][$sha1]['complete'];
                        $leechers = $scrape_data['files'][$sha1]['incomplete'];
                    } else {
                        $seeders = $scrape_data['complete'];
                        $leechers = $scrape_data['incomplete'];
                    }
                    $data = array(
                        'seeders' => $seeders,
                        'leechers' => $leechers,
                        'cached' => date('Y-m-d H:i:s')
                    );
                    ttdb_update($torrent['file_id'], $data);
                    $htmlcode.="<li>";
                    $htmlcode.="<a href = '" . $torrent['url'] . "' > " . $torrent['title'] . "</a>: ";
                    $htmlcode.="(" . $torrent['size_mb'] . " Мб) ";
                    $htmlcode.="Раздают: " . $seeders;
                    $htmlcode.=" <span style = 'color:silver'>Неполностью: " . $leechers . "</span>";
                    $htmlcode.="</li>";
                }
            } else {
                $sha1 = base64_decode($torrent['sha1']);
                $scrape_data = bdecode($torrent_file_contents);
                $seeders = $torrent['seeders'];
                $leechers = $torrent['leechers'];
                $htmlcode.="<li>";
                $htmlcode.="<a href = '" . $torrent['url'] . "' > " . $torrent['title'] . "</a>: ";
                $htmlcode.="(" . $torrent['size_mb'] . " Мб) ";
                $htmlcode.="Раздают: " . $seeders;
                $htmlcode.=" <span style = 'color:silver'>Неполностью: " . $leechers . "</span>";
                $htmlcode.="</li>";
            }
        }
    }
    $htmlcode.="</ol>";
    $htmlcode.=$options['html_after'];
    return $htmlcode;
}

function replace_content(
$content) {
    $content = str_replace('<!-- TorrentTracker -->', selectFormOfView(), $content);
    $content = str_replace('<!-- TorrentTrackerPost -->', generateTrackerPostHTML(), $content);
    return $content;
}

add_action('add_attachment', 'torrent_add_attachment');
add_action('delete_attachment', 'torrent_delete_attachment');
add_action('edit_attachment', 'torrent_edit_attachment');
add_filter('the_content', 'replace_content');
register_activation_hook(__FILE__, 'ttdb_install');

include "torrent_tracker_adm.php";
?>
